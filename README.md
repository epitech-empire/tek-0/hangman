# Hangman

This game was made to show the fate that awaits skateboarders who dare to venture at Epitech

Everyone knows the rules of this game but I'm going to specify some of them :

- the word is chosen from the database or entered by the player
- the player can make 6 mistakes before losing
- the correct letters are displayed where they appear in the word
- the incorrect letters are displayed below
